package controllers

import (
    "fmt"
    "github.com/gofiber/fiber/v2"
    "github.com/google/uuid"
    "github.com/lambda-platform/lambda/DB"
    agentUtils "github.com/lambda-platform/lambda/agent/utils"
    "golang.org/x/crypto/bcrypt"
    "lambda/app/models"
    "net/http"
)

func GetAllUsers(c *fiber.Ctx) error {
    var users []models.Users
    DB.DB.Find(&users)

    return c.JSON(users)
}

func Register(c *fiber.Ctx) error {
    userRegisterReq := models.Users{}
    err := c.BodyParser(&userRegisterReq)
    if err != nil {
        fmt.Println(err.Error())
        return c.Status(http.StatusInternalServerError).JSON("server error")
    }

    userData := models.Users{}
    userDataEmail := models.Users{}
    userDataLogin := models.Users{}

    DB.DB.Where("email = ?", userRegisterReq.Email).Find(&userDataEmail)
    DB.DB.Where("login = ?", userRegisterReq.Login).Find(&userDataLogin)

    if userDataEmail.ID > 0 {
        return c.Status(http.StatusOK).JSON(map[string]string{
            "status":  "warning",
            "message": "Бүртгэлтэй е-майл хаяг байна.",
        })
    } else if userDataLogin.ID > 0 {
        return c.Status(http.StatusOK).JSON(map[string]string{
            "status":  "warning",
            "message": "Нэвтрэх нэр бүртгэлтэй байна. ",
        })
    }

    userData.Login = userRegisterReq.Login
    userData.Email = userRegisterReq.Email
    userData.FirstName = userRegisterReq.FirstName
    userData.LastName = userRegisterReq.LastName
    userData.RegisterNumber = uuid.New().String()
    password, _ := agentUtils.Hash(userRegisterReq.Password)
    userData.Password = password
    userData.Role = 2
    userData.Status = "2"

    AddRole(*userData.FirstName)

    DB.DB.Create(&userData)

    return c.Status(http.StatusOK).JSON(map[string]string{
        "status":  "success",
        "message": "Бүртгэл амжилттай",
    })
}

func UpdateUser(c *fiber.Ctx) error {
    userRegisterReq := models.Users{}
    err := c.BodyParser(&userRegisterReq)
    if err != nil {
        fmt.Println(err.Error())
        return c.Status(http.StatusInternalServerError).JSON("server error")
    }

    authUser := agentUtils.AuthUserUUID(c)
    user := models.Users{}
    DB.DB.Where("id = ?", authUser.ID).First(&user)

    if user.ID == 0 {
        return c.Status(http.StatusOK).JSON(map[string]string{
            "status":  "warning",
            "message": "Хэрэглэгч олдсонгүй",
        })
    }

    DB.DB.Model(&models.Users{}).Where("id = ?", authUser.ID).Updates(
        models.Users{
            FirstName: userRegisterReq.FirstName,
            LastName:  userRegisterReq.LastName,
        },
    )

    return c.Status(http.StatusOK).JSON(map[string]string{
        "status":  "success",
        "message": "Мэдээлэл засагдсан",
    })
}

func UpdatePassword(c *fiber.Ctx) error {
    user := models.Users{}
    reqPass := models.PassReq

    if err := c.BodyParser(&reqPass); err != nil {
        return c.Status(http.StatusBadRequest).JSON(fiber.Map{
            "status":  "error",
            "message": "Invalid JSON request body",
        })
    }

    authUser := agentUtils.AuthUserUUID(c)

    if result := DB.DB.First(&user, authUser.ID); result.Error != nil {
        return c.Status(http.StatusNotFound).JSON(fiber.Map{
            "status":  "error",
            "message": "Хэрэглэгч олдсонгүй",
        })
    }

    if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(reqPass.OldPassword)); err != nil {
        return c.Status(http.StatusBadRequest).JSON(fiber.Map{
            "status":  "error",
            "message": "Хуучин нууц үг таарахгүй байна",
        })
    }

    if reqPass.NewPassword != reqPass.RetypePassword {
        return c.Status(http.StatusBadRequest).JSON(fiber.Map{
            "status":  "error",
            "message": "Нууц үг баталгаажуулалт таарсангүй",
        })
    }

    newHashedPassword, err := bcrypt.GenerateFromPassword([]byte(reqPass.NewPassword), bcrypt.DefaultCost)
    if err != nil {
        return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
            "status":  "error",
            "message": "Нууц hash failed",
        })
    }

    if result := DB.DB.Model(&user).Update("password", string(newHashedPassword)); result.Error != nil {
        return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
            "status":  "error",
            "message": "Нууц үг солиход алдаа гарлаа",
        })
    }

    return c.Status(http.StatusOK).JSON(fiber.Map{
        "status":  "success",
        "message": "Нууц үг солигдлоо",
    })

}

