package models

import (
    "gorm.io/gorm"
    "time"
)

type Roles struct {
    ID          int            `gorm:"column:id;primaryKey;autoIncrement" json:"id"`
    Name        string         `gorm:"column:name" json:"name"`
    DisplayName string        `gorm:"column:display_name" json:"display_name"`
    Description *string        `gorm:"column:description" json:"description"`
    Permissions *string        `gorm:"column:permissions" json:"permissions"`
    Extra       *string        `gorm:"column:extra" json:"extra"`
    Menu        *string        `gorm:"column:menu" json:"menu"`
    CreatedAt   *time.Time     `gorm:"column:created_at" json:"created_at"`
    UpdatedAt   *time.Time     `gorm:"column:updated_at" json:"updated_at"`
    DeletedAt   gorm.DeletedAt `gorm:"column:deleted_at" json:"deleted_at"`
}

func (r *Roles) TableName() string {
    return "roles"
}
