package models

import (
	//"github.com/lambda-platform/lambda/DB"
	"gorm.io/gorm"
	"time"
)

type Users struct {
	ID             int            `gorm:"column:id;primaryKey;autoIncrement" json:"id"`
	Status         string         `gorm:"column:status" json:"status"`
	Role           int            `gorm:"column:role" json:"role"`
	Login          string         `gorm:"column:login" json:"login"`
	Email          string         `gorm:"column:email" json:"email"`
	RegisterNumber string         `gorm:"column:register_number" json:"register_number"`
	Avatar         *string        `gorm:"column:avatar" json:"avatar"`
	Bio            *string        `gorm:"column:bio" json:"bio"`
	FirstName      *string        `gorm:"column:first_name" json:"first_name"`
	LastName       *string        `gorm:"column:last_name" json:"last_name"`
	Birthday       *time.Time     `gorm:"column:birthday" json:"birthday"`
	Phone          *string        `gorm:"column:phone" json:"phone"`
	Gender         *string        `gorm:"column:gender" json:"gender"`
	Password       string         `gorm:"column:password" json:"password"`
	CreatedAt      *time.Time     `gorm:"column:created_at" json:"created_at"`
	UpdatedAt      *time.Time     `gorm:"column:updated_at" json:"updated_at"`
	DeletedAt      gorm.DeletedAt `gorm:"column:deleted_at" json:"deleted_at"`
}

func (u *Users) TableName() string {
	return "users"
}

var PassReq struct {
	OldPassword    string `json:"oldPassword"`
	NewPassword    string `json:"newPassword"`
	RetypePassword string `json:"retypePassword"`
}

//type UsersData struct {
//    ID             int            `gorm:"column:id;primaryKey;autoIncrement" json:"id"`
//    Password       string         `gorm:"column:password" json:"password"`
//}
//
//func (u *UsersData) TableName() string {
//    return "users"
//}
