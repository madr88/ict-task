package routes

import (
	"github.com/gofiber/fiber/v2"
	_ "github.com/gofiber/jwt/v3"
	_ "github.com/golang-jwt/jwt/v4"
	"github.com/lambda-platform/lambda/agent/agentMW"
	"lambda/app/controllers"
)

func Api(e *fiber.App) {
	a := e.Group("/api")
	a.Get("/users", controllers.Users)
	a.Get("/send-notification", controllers.SendNotification)
	a.Post("/grid-form-example", controllers.GridFromExample)
	//a.Get("/read-icons", controllers.ReadIcons)

	//	User Table Actions
	a.Get("/get-users", controllers.GetAllUsers)
	a.Post("/register-users", controllers.Register)
	a.Put("/update-user", agentMW.IsLoggedIn(), controllers.UpdateUser)
	a.Put("/update-password", agentMW.IsLoggedIn(), controllers.UpdatePassword)
}
